const js2xmlparser = require("js2xmlparser");
const fs = require("fs");
const _ = require("lodash");

function clean(s) {
  return (s && s.replace(/\s/g, "")) || s;
}

class GludParser {
  constructor(glud) {
    this._glud = "M=({S,A,B,qf},{a,b},T,S,{qf})";
    this.gludRexg = /^[A-M]=\(\{((([A-Z|a-z]+)\,?(\s+)?)+)\},(\s+)?\{((([0-9|a-z]+)\,?(\s+)?)+)\}\,(\s+)?((([A-Z|a-z]+)\,?(\s+)?)+)\{([a-z]+)\}\)$/;
    this.glud = clean(glud);
    const match = (glud.match(this.gludRexg) || []).map(s => clean(s));
    if (!match[1] || !match[6] || !match[11] || !match[15]) {
      throw new Error("Formato inválido da GLUD, ex: " + this._glud);
    }
    this.rules = [];
    this.transitions = [];
    this.usedRules = {};
    this.terminalVar = match[2];
    this.usedRulesGetOnly = {};
    this.finalValue = match[15];
    this.vars = match[1].split(",");
    this.terminalConj = ["o"].concat(match[6].split(","));
  }

  getVars() {
    return this.vars.filter(v => 
      v !== this.finalValue);
  }

  generateXml(filename, state, transition) {
    const xmlJson = {
      type: "fa",
      automaton: {
        state,
        transition
      }
    };
    const automato = js2xmlparser.parse("structure", xmlJson, {
      declaration: {
        encoding: "UTF-8",
        standalone: "no"
      }
    });
    fs.writeFileSync(filename, automato);
    console.log("file saved.");
    return automato;
  }

  parseRule(rule) {
    const ruleParsed = rule
      .match(/^([A-Z|a-z])(\s+)?\-\>(\s+)?([A-Z|a-z|0-9|\s+]+)$/)
      .filter(s => s !== undefined && s !== null);
    if (!ruleParsed || ruleParsed.length < 3) {
      throw "Regra fora do formato. Ex: S -> aB|a";
    }
    const [full, variable, values] = ruleParsed;
    return values.split("|").map(value => {
      if (!this.vars.includes(variable) && variable != "S") {
        throw `Regra fora do formato. Variável '${variable}' não está definida na GLUD`;
      }
      const terms =
        this.terminalConj
          .map(item => {
            const m = value.match(item);
            return m ? m[0] : null;
          })
          .filter(item => !_.isNil(item)) || [];
      const vars =
        this.vars
          .map(item => {
            const m = value.match(item);
            return m ? m[0] : null;
          })
          .filter(item => !_.isNil(item)) || [];

      vars.forEach((to, index) => {
        const from = variable;
        const read = terms[index] || "ε";
        this.transitions.push({
          read,
          from,
          to
        });
      });
      if (!vars.length && !terms.length) {
        throw `Regra fora do formato. valuesnão está definida na GLUD`;
      }
      return [variable, values];
    });
  }

  addRule(rule) {
    rule = clean(rule);
    if (this.rules.length === 0 && rule[0] !== "S") {
      throw "A primeira regra deve ser da variável inicial S";
    }
    const ruleParsed = this.parseRule(rule);
    ruleParsed.forEach(r => this.rules.push(r));
  }

  export(filename) {
    const xBase = 120;
    const yBase = 80;
    const states = this.vars.map((s, i) => ({
      "@": {
        id: i,
        name: "q" + i,
        value: s
      },
      x: xBase * (i + 1),
      y: yBase,
      ...(s === "S" ? { initial: {} } : {}),
      ...(s === this.finalValue ? { final: {} } : {})
    }));

    this.generateXml(
      filename,
      states,
      this.transitions.map(({ to, read, from }) => {
        return {
          read,
          to: _.findIndex(states, ["@.value", to]),
          from: _.findIndex(states, ["@.value", from])
        }
      })
    );
  }
}
module.exports = GludParser;
const glud = new GludParser("M=({S,A,B,qf}, {a,b}, T, S,{qf})");
["S->aA", "A->bB", 
// "A->qf", 
"B->qf"].map(rule => glud.addRule(rule));
console.log(glud.getVars());
glud.export("teste.jff");
