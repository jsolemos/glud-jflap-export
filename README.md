### Gera um arquivo do JFLAP a partir de uma GLUD e regras

    $ node ./index

### Converte um AFND para AFD a partir de um arquivo exportado do JFlap

    $ node ./afndConverter ./examples/afnd.jff
