const js2xmlparser = require("js2xmlparser");
const parser = require("xml2json");
const fs = require("fs");
const _ = require("lodash");
const cTable = require("console.table");
const path = require("path");
// const { primisefy } = require("util");
const args = process.argv.slice(2);
class AfndConverter {
  constructor(filename) {
    this.filename = path.basename(filename);
    this.xml = fs.readFileSync(filename);
    this.table = [["Ø", "Ø", "Ø"]];
    this.initialsStates = [];
    this.finalTables = [];
  }
  async init() {
    this.afnd = JSON.parse(parser.toJson(this.xml.toString()));
    this.genTable();
  }

  genTable() {
    const { state, transition } = this.afnd.structure.automaton;
    this.state = state;
    this.transition = transition;
    // recupera o alfabeto
    this.values = _.uniq(transition.map(o => o.read));
    const getState = s => {
      let label = s.name;
      return label || "Ø";
    };
    // Gera a tabela com os estados simples
    this.table = this.table.concat(
      state.map((s, index) => {
        const ret = {};
        this.values.forEach(v => {
          const nexStates = _.map(
            transition.filter(o => o.from == index.toString() && o.read == v),
            "to"
          ).map(o => state[o].name);
          ret[v] = _.uniq(nexStates.sort()).join(",") || "Ø";
        });

        const row = [getState(s), ...Object.values(ret)];
        console.log(row);
        return row;
      })
    );

    const genAditionasRows = () => {
      // Gera a tabela com os estados em conjuntos
      _.flatten(this.table).forEach(newState => {
        if (newState !== "") {
          const index = this.table
            .map(o => o[0])
            .findIndex(o => {
              return (
                o.replace(/(\>|\*)/, "") === newState.replace(/(\>|\*)/, "")
              );
            });
          if (index === -1) {
            const ret = {};
            this.values.forEach(v => {
              const nexStates = _.map(
                transition.filter(
                  o =>
                    newState
                      .split(",")
                      .map(x => state.findIndex(z => z.name === x).toString())
                      .includes(o.from) && o.read == v
                ),
                "to"
              ).map(o => state[o].name);
              ret[v] = _.uniq(nexStates.sort()).join(",") || "Ø";
            });
            const row = [newState, ...Object.values(ret)];
            console.log(row);
            this.table.push(row);
            genAditionasRows();
          }
        }
      });
    };
    genAditionasRows();
    this.converter();
    // this.table.unshift(["Estado", ...values])
  }

  /**
   * Gera a tablea de conversão
   */
  converter() {
    const mapStates = {};
    this.mapNewStates = {};
    const newTableStates = this.table.reduce((prev, current, index, array) => {
      const tmp = [...current];
      const stateName = String.fromCharCode(65 + index);
      mapStates[tmp[0]] = stateName;
      this.mapNewStates[stateName] = tmp[0];
      return [...prev, [stateName, ...tmp.splice(1)]];
    }, []);

    const newTable = this.table.reduce((prev, current, index, array) => {
      const tmp = [...current];
      const state = prev[index][0];
      prev[index] = [state, ...tmp.splice(1).map(o => mapStates[o] || o)];
      return prev;
    }, newTableStates);

    console.log("TABELAS");
    this.printTable(this.table);
    this.printTable(newTable);
    this.export(newTable);
  }

  printTable(rows) {
    console.table(["Estado", ...this.values], rows);
  }

  export(table) {
    const initials = this.state.filter(x => x.initial).map(x => x.name);
    const final = this.state.filter(x => x.final).map(x => x.name);
    const states = table.reduce((prev, current, index) => {
      prev[current[0]] = current.slice(1);
      return prev;
    }, {});
    const useds = [];
    const finalTable = [];
    const getNextState = (row, stateName) => {
      return row.filter((current, pos) => {
        const isFinal = final.find(
          x => this.mapNewStates[current].indexOf(x) !== -1
        );
        if (!useds.includes(current)) {
          useds.push(current);
          finalTable.push([stateName, ...row]);
          return getNextState(states[current], current);
        }
        return false;
      });
    };
    table
      .filter(o => {
        // Filtra os dados da tabela pelos estados iniciais
        return initials.find(x => this.mapNewStates[o[0]] === x);
      })
      .forEach((row, index) => {
        getNextState(row.slice(1), row[0]);
      });

    this.printTable(finalTable);
    const xBase = 120;
    const yBase = 80;
    const state = finalTable.map((row, i) => {
      const label = row[0];
      return {
        "@": {
          value: label,
          name: "S" + i,
          id: i
        },
        label: `${this.mapNewStates[label]}(${label})`,
        x: xBase * (i + 1),
        y: yBase,
        ...(initials.find(x => this.mapNewStates[label] === x)
          ? { initials: {} }
          : {}),
        ...(final.find(x => this.mapNewStates[label].indexOf(x) !== -1)
          ? { final: {} }
          : {})
      };
    });
    const transition = _.flatten(
      this.values.map((v, pos) => {
        return [
          ...finalTable.map((row, i) => {
            const label = row[0];
            return {
              read: v,
              from: state.findIndex(x => x["@"].value === label),
              to: state.findIndex(x => {
                return x["@"].value === row[pos + 1];
              })
            };
          })
        ];
      })
    ).filter(i => i.to !== -1);

    const xmlJson = {
      type: "fa",
      automaton: {
        state: state.filter((x, i) => {
          return transition.find(t => {
            return [t.to, t.from].includes(i);
          });
        }),
        transition: transition
      }
    };
    const xml = js2xmlparser.parse("structure", xmlJson, {
      declaration: {
        encoding: "UTF-8",
        standalone: "no"
      }
    });
    const fileout = `${this.filename.split(".")[0]}-afd-${
      new Date().toISOString().split("T")[0]
    }.jff`;

    fs.writeFileSync(fileout, xml);
    console.info(`AFD saved in: ${path.resolve(fileout)}`);
  }
}
new AfndConverter(args[0] || "example/afnd.jff")
  .init()
  .catch(err => console.error(err));
