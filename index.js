const GludParser = require("./gludParser");
const readlineSync = require("readline-sync");

let v = 1;
let glud;

while (v === 1) {
  const gludText = readlineSync.question(
    "Insira uma GLUD para geração do arquivo. ex: M=({S,A,B,qf}, {a,b}, T, S,{qf})\n-> "
  );
  try {
    glud = new GludParser(gludText);
    v = 2;
  } catch (error) {
    console.error("Erros foram encontrados: " + error.message);
    continue;
  }
}
v = 1;
let ruleText;
const vars = glud.getVars();

for (let i = 0; i < vars.length; i++) {
  const V = vars[i];
  // console.log({V});
  ruleText = readlineSync.question(
    `Insira uma regra de produção da variável ${V}, ex: aA\n-> `
  );
  if (!ruleText && V === "S") {
    i--;
    console.error("Informe a regra de S");
    continue;
  } else {
    ruleText = "qf";
  }
  const rule = `${V}->${ruleText}`;
  try {
    glud.addRule(rule);
  } catch (error) {
    console.error("Erros foram encontrados: " + error.message);
    i--;
    continue;
  }
}
// while (v === 1) {
//   ruleText = readlineSync.question(
//     "Insira uma regra de produção, ex: S->aA\n-> "
//   );
//   console.log(ruleText);
//   if (!ruleText) {
//     continue;
//   }
//   try {
//     glud.addRule(ruleText);
//   } catch (error) {
//     console.error("Erros foram encontrados: " + error.message);
//     continue;
//   }

//   ruleText = readlineSync.question(
//     "Deseja inserir nova regra? [1] - sim [0] - não\n-> "
//   );
//   if (!ruleText) {
//     continue;
//   }
//   v = Number(ruleText);
// }

filename = readlineSync.question(
  "Com qual nome deseja salvar o arquivo .jff? padrão: automaton.jff \n-> "
);

glud.export(filename || "automaton.jff");
